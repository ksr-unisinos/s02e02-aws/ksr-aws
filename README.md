# KSR-AWS

Elastic Compute Cloud - EC2

### Link da conta grátis de estudante
https://www.awseducate.com

### Tutorial para usar o putty
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html

> Coisas Legais:

Node-Red - Ubuntu
https://nodered.org/docs/getting-started/aws

Node-Red - Amazon Linux:
https://nodered.org/docs/getting-started/local#installing-with-npm

### Video simples
[![SC2 Video](https://img.youtube.com/vi/SxPOUtxBlcE/0.jpg)](http://www.youtube.com/watch?v=SxPOUtxBlcE)

```
#!/bin/bash
sudo mkdir /home/ec2-user/.node-red
sudo chmod 777 /home/ec2-user/.node-red
sudo yum update -y
sudo yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_14.x | sudo -E bash -
sudo yum install -y nodejs
sudo npm install -g --unsafe-perm node-red
sudo wget -O /etc/systemd/system/nodered.service https://raw.githubusercontent.com/itirohidaka/NodeRed/master/Node-Red.service
sudo systemctl start nodered.service
sudo systemctl enable nodered.service
```

### Segurança
https://nodered.org/docs/user-guide/runtime/securing-node-red

`cd .node-red`

`vim settings.js`

#### comandos do vim:
busca palavra

`:/adminAuth`

edita o documento

`i`

sai do editor

`ESC`

salva e sai

`:wq`

### Cria uma senha nova

`node-red admin hash-pw`




### instalar htop
`sudo yum install htop`


### MQTT - Mosquitto
https://www.whizitservices.com/installing-the-mosquitto-mqtt-messaging-broker-and-node-red-client-on-centos-7/

### abre as portas 8080 e 1883 para o mqtt

`sudo amazon-linux-extras install epel`

`sudo yum install mosquitto`

### inicia o serviço
`sudo service mosquitto start`

### adicionad op serviço na inicialização
`sudo systemctl enable mosquitto`

### Adiciona o flow e configura
https://flows.nodered.org/node/node-red-contrib-mqtt-broker


### configura o arduino
https://www.arduinoecia.com.br/enviando-mensagens-mqtt-modulo-esp32-wifi/

### Influxdb

https://techviewleo.com/how-to-install-influxdb-on-amazon-linux/

Adicionar o influx ao gerenciador de pacotes yum

```
cat <<EOF | sudo tee /etc/yum.repos.d/influxdb.repo
[influxdb]
name = InfluxDB Repository - RHEL 7
baseurl = https://repos.influxdata.com/rhel/7/x86_64/stable
enabled = 1
gpgcheck = 1
gpgkey = https://repos.influxdata.com/influxdb.key
EOF
```


instalar o influxdb

`sudo yum install influxdb`

inicia

`sudo systemctl start influxdb`

adiciona ele na inicilização

`sudo systemctl enable influxdb`


configuando o influxdB

https://funprojects.blog/2020/02/01/influxdb-with-node-red/




### Interface gráfica
https://aws.amazon.com/pt/premiumsupport/knowledge-center/ec2-linux-2-install-gui/


